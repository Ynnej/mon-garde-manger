//
//  DetailTableViewController.swift
//  Mon garde manger
//
//  Created by Jennifer on 18/04/2016.
//  Copyright © 2016 Jennifer Guiard. All rights reserved.
//

import UIKit
import RealmSwift

class DetailTableViewController: UITableViewController {
    
    var selectedType :String = ""
    var isFrozen :Bool = false
    var foodList :Array<FoodElement> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set title of navBar
        self.navigationItem.title = selectedType
        //TODO: Rename backbutton
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    @IBAction func addFoodElement(sender: UIBarButtonItem) {
        //TODO: Present a view to add a product (allow multiple add)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodList.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120.0 //TODO: Adapt in function of the content and window size
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: DetailCell = tableView.dequeueReusableCellWithIdentifier("DetailCell", forIndexPath: indexPath) as! DetailCell
        
        let anElement :FoodElement = foodList[indexPath.row]
        cell.productName?.text = anElement.name
        cell.dueDate?.text = convertDateIntoString(anElement.dueDate)
        cell.productImage?.image = UIImage(named: anElement.imageName)

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
