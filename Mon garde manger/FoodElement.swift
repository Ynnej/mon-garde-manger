//
//  FoodElement.swift
//  Mon garde manger
//
//  Created by Jennifer on 09/05/2016.
//  Copyright © 2016 Jennifer Guiard. All rights reserved.
//

import RealmSwift

class FoodElement : Object{
    
    //Food element default values
    //TODO: Take care to identify an element not use name only!!
    
    dynamic var name :String = "";
    
    dynamic var dueDate :NSDate = NSDate.distantPast();
    
    dynamic var imageName :String = "";
    
    dynamic var isFrozen :Bool = false;
    
    dynamic var type :String = "";
}
