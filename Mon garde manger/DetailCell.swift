//
//  DetailCell.swift
//  Mon garde manger
//
//  Created by Jennifer on 18/04/2016.
//  Copyright © 2016 Jennifer Guiard. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    
    @IBOutlet weak var productName :UILabel!
    @IBOutlet weak var dueDate :UILabel!
    @IBOutlet weak var productImage :UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
