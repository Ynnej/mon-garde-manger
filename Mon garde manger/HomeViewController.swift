//
//  HomeViewController.swift
//  Mon garde manger
//
//  Created by Jennifer on 18/04/2016.
//  Copyright © 2016 Jennifer Guiard. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var frozenSelector: UISegmentedControl!
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    var detailViewController: DetailTableViewController = DetailTableViewController()
    
    //TODO : Replace by an enum
    var listOfCategories: Array<String> = ["Légumes","Fruits","Viandes","Poissons","Plats cuisinés","Desserts"]
    var selectedIndex :Int = 0
    var frozenSelected :Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set default state
        frozenSelector.selectedSegmentIndex = 0
        frozenSelected = true
    }
    
    @IBAction func selectedSegmentChange(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            frozenSelected = true
        case 1:
            frozenSelected = false
        default:
            break
        }
    }
    
    // MARK: - CollectionView datasource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfCategories.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: HomeCell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCell", forIndexPath: indexPath) as! HomeCell
        cell.cellTitle.text = listOfCategories[indexPath.row]
        
        return cell
    }
    
    // MARK: - CollectionView delegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        selectedIndex = indexPath.row
        performSegueWithIdentifier("presentDetailVC", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "presentDetailVC"{
            let vc = segue.destinationViewController as! DetailTableViewController
            let selectedFoodType: String = listOfCategories[selectedIndex]
            vc.selectedType = selectedFoodType
            vc.isFrozen = frozenSelected
            let filter :NSPredicate = NSPredicate(format: "type = %@ AND isFrozen = %@", selectedFoodType,NSNumber(bool: frozenSelected))
            vc.foodList = DataBaseAccessMethod().getFoodElementsWithFilter(filter)
        }
    
    }
    
}
