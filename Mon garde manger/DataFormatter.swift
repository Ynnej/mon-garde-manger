//
//  DataFormatter.swift
//  Mon garde manger
//
//  Created by Jennifer on 09/05/2016.
//  Copyright © 2016 Jennifer Guiard. All rights reserved.
//

import Foundation

func convertDateIntoString(date: NSDate) -> String {
    let dateFormater : NSDateFormatter = NSDateFormatter()
    dateFormater.dateFormat = "dd/MM/yyyy"
    print(dateFormater.stringFromDate(date))
    return dateFormater.stringFromDate(date)
}
