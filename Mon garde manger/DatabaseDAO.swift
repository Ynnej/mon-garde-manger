//
//  DatabaseDAO.swift
//  Mon garde manger
//
//  Created by Jennifer on 09/05/2016.
//  Copyright © 2016 Jennifer Guiard. All rights reserved.
//

import Foundation
import RealmSwift

class DataBaseDAO { //TODO: Add print for errors
    
    func getFoodElementsWithFilter(filter: NSPredicate) -> Results<FoodElement> {
        // Get the default Realm
        let defaultRealm = try! Realm()
        let result = defaultRealm.objects(FoodElement).filter(filter)
        return result
    }
    
    func addFoodElements(listOfElements:Array<FoodElement>) {
        // Get the default Realm
        let defaultRealm = try! Realm()
        
        for anElement in listOfElements {
            try! defaultRealm.write {
                defaultRealm.add(anElement)
            }
        }
        
    }
    
    func removeFoodElements(listOfElements:Array<FoodElement>) {
        // Get the default Realm
        let defaultRealm = try! Realm()
        
        for anElement in listOfElements {
            try! defaultRealm.write {
                defaultRealm.delete(anElement)
            }
        }
    }
    
    func countAllFoodElementsOfType(type: String) -> Int {
        // Get the default Realm
        let defaultRealm = try! Realm()
        let predicate = NSPredicate(format: "type = %@",type)
        let result = defaultRealm.objects(FoodElement).filter(predicate)
        return result.count
    }
}

