//
//  DatabaseAccessMethod.swift
//  Mon garde manger
//
//  Created by Jennifer on 09/05/2016.
//  Copyright © 2016 Jennifer Guiard. All rights reserved.
//

import Foundation
import RealmSwift

class DataBaseAccessMethod {
    
    private let DAO :DataBaseDAO = DataBaseDAO()
    
    func getFoodElementsWithFilter(filter: NSPredicate) -> Array<FoodElement> {
        return Array(DAO.getFoodElementsWithFilter(filter))
    }
    
    func addFoodElements(listOfElements:Array<FoodElement>) {
        DAO.addFoodElements(listOfElements)
    }
    
    func removeFoodElements(listOfElements:Array<FoodElement>) {
        DAO.removeFoodElements(listOfElements)
    }
    
    func countAllFoodElementsOfType(type: String) -> Int {
        return DAO.countAllFoodElementsOfType(type)
    }
}
